package model;

public class Warehouse {
	private Integer warehouse_id;
	private String name;
	private Integer city_id;
	
	public Warehouse(Integer warehouse_id, String name, Integer city_id) {
		this.warehouse_id = warehouse_id;
		this.name = name;
		this.city_id = city_id;
	}
	
	public Warehouse(String name, Integer city_id) {
		this.name = name;
		this.city_id = city_id;
	}
	
	public Integer getId() {
		return this.warehouse_id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Integer getCityId() {
		return this.city_id;
	}
}
