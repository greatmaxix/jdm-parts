import React, { Component } from 'react'
import './login.css';
import BackgroundSlider from 'react-background-slider'
 
import image1 from './../assets/Silvia.jpg';
import image2 from './../assets/Supra.jpg';
import image3 from './../assets/Skyline.jpg';
import Header from './Header';
 
export default class MainPage extends Component {
  render () {
    return (
        <div>
            <div className="main-card-container">
                <div className="main-card">
                    <header>
                        <h3>JDM parts</h3>
                        <h5>Best JDM parts around the world</h5>
                        <h6>Simple, fast, beautiful</h6>
                    </header>
                    <a href="/parts" className="btn btn-outline-info" type="button">Check what we have!</a>
                </div> 
            </div>
            <BackgroundSlider
                images={[image1, image2, image3]}
                duration={5}
                transition={2}
            />
        </div>
    )
  }
}