import React from 'react';
import './login.css';
import axios from 'axios';
import Header from './Header';
import { JavaCities } from './Cities';

export type JavaPartManufacturer = {
    id?: number,
    name: string,
    desc: string,
    city_id: number
}

type PartManufacturerState = {
    partmans: JavaPartManufacturer[],
    cities: JavaCities[],
    selected_partman?: JavaPartManufacturer,
    new_partman: JavaPartManufacturer,
}

export default class PartManufacturer extends React.Component<any, PartManufacturerState> {
    constructor(props: any) {
        super(props);
        this.state = {
            partmans: [],
            cities: [],
            new_partman: {
                name: "",
                desc: "",
                city_id: -1,
            }
        };
    }

    async reload() {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/partmanufacturer',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.retrieveData(response.data);
        })
        .catch((error: any) => {
            console.log(error);
        });

        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/city',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.fillCitiesList(response.data);
        })
            .catch((error: any) => {
                console.log(error);
            });
    }

    retrieveData(data: any) {
        let partmans: JavaPartManufacturer[] = data;
        this.setState({
            partmans
        })
    }

    fillCitiesList(data: any) {
        let cities: JavaCities[] = data;
        this.setState({
            cities,
        })
    }

    insertNewPartMan() {
        axios({
            method: 'put',
            url: 'http://127.0.0.1:8000/api/partmanufacturer',
            headers: {},
            data: this.state.new_partman
        }).then((response: any) => {
            console.log(response);
            window.location.reload(false);
        })
            .catch((error: any) => {
                console.log(error);
        });
    }

    updatePartMan() {
        if (this.state.selected_partman) {
            axios({
                method: 'post',
                url: 'http://127.0.0.1:8000/api/partmanufacturer',
                headers: {},
                data: this.state.selected_partman
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
                .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    deletePartMan() {
        if (this.state.selected_partman) {
            axios({
                method: 'delete',
                url: 'http://127.0.0.1:8000/api/partmanufacturer',
                headers: {},
                params: { partid: + this.state.selected_partman.id!}
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
            .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    componentWillMount() {
        console.log(localStorage.getItem("username"));
        if (!localStorage.getItem("username")) {
            this.props.history.push("/");
        }
        else {
            this.reload();
        }
    }

    handleInputChange(event: any) {
        let new_partman: JavaPartManufacturer = this.state.new_partman;
        let selected_partman: any = this.state.selected_partman;
        if (event.target && event.target.name === 'newPartManName') {
            new_partman.name = event.target.value;
        }
        else if (event.target && event.target.name === 'newPartManDesc') {
            new_partman.desc = event.target.value;
        }
        else if (event.target && event.target.name === 'newPartManCity') {
            new_partman.city_id = event.target.value;
        }

        if (selected_partman) {
            if (event.target && event.target.name === 'updatePartManName') {
                selected_partman.name = event.target.value;
            }
            else if (event.target && event.target.name === 'updatePartManDesc') {
                selected_partman.desc = event.target.value;
            }
            else if (event.target && event.target.name === 'updatePartManCity') {
                selected_partman.city_id = event.target.value;
            }
        }
        this.setState({
            new_partman,
            selected_partman
        })
    }

    onPartManEditSelected(partman_id: any) {
        if (partman_id === undefined) {
            this.setState({
                selected_partman: undefined
            })
            return;
        }

        let foundPart = this.state.partmans.find((element: JavaPartManufacturer) =>
            partman_id === element.id
        );
        if (foundPart) {
            this.setState({
                selected_partman: foundPart
            })
        }
    }

    render() {
        console.log("this.state", this.state);
        return (
            <div><Header currUrl="partman"></Header>
            <div className="container pt-5" >
                <div className="row login-row">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Part manufacturer id</th>
                                <th scope="col">Part manufacturer name</th>
                                <th scope="col">Part manufacturer description</th>
                                <th scope="col">City</th>
                                <th scope="col"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartManEditSelected.bind(this, undefined)}>Clear selection</button></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.partmans.map((part: JavaPartManufacturer) => {
                                return (
                                    <tr className={this.state.selected_partman && this.state.selected_partman.id === part.id ? "bg-info" : ""}>
                                        <th scope="row">{part.id}</th>
                                        <th>{part.name}</th>
                                        <th>{part.desc}</th>
                                        <th>{(this.state.cities.find((el)=>{return el.city_id == part.city_id}))?.name}</th>
                                        <th scope="row"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartManEditSelected.bind(this, part.id)}>Select to edit/delete</button></th>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                {this.state.selected_partman ? 
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <button className="btn btn-outline-secondary" type="button" onClick={this.updatePartMan.bind(this)}>Update</button>
                            <button className="btn btn-outline-secondary" type="button" onClick={this.deletePartMan.bind(this)}>Delete</button>
                        </div>
                        <input type="text" name="updatePartManName" placeholder={this.state.selected_partman.name} onChange={this.handleInputChange.bind(this)} className="form-control" />
                        <input type="text" name="updatePartManDesc" placeholder={this.state.selected_partman.desc} onChange={this.handleInputChange.bind(this)} className="form-control" />
                        <select className="custom-select" name="updatePartManCity" onChange={this.handleInputChange.bind(this)}>
                                {
                                    this.state.cities.map((city: JavaCities) => {
                                        if (city.city_id === this.state.selected_partman!.city_id) {
                                            return (<option selected value={city.city_id}>{city.name}</option>)
                                        }
                                        else {
                                            return (
                                                <option value={city.city_id}>{city.name}</option>
                                            )
                                        }
                                    })
                                }
                            </select>
                    </div>
                    :
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <button className="btn btn-outline-secondary" type="button" onClick={this.insertNewPartMan.bind(this)}>Insert</button>
                        </div>
                        <input type="text" name="newPartManName" placeholder="New part manufacturer name" onChange={this.handleInputChange.bind(this)} className="form-control" />
                        <input type="text" name="newPartManDesc" placeholder="New part manu. description" onChange={this.handleInputChange.bind(this)} className="form-control" />
                        <select className="custom-select" name="newPartManCity" onChange={this.handleInputChange.bind(this)}>
                            <option selected>New part manufacturer`s city</option>
                                {
                                    this.state.cities.map((city: JavaCities) => {
                                        return (
                                            <option value={city.city_id}>{city.name}</option>
                                        )
                                    })
                                }
                            </select>
                    </div>
                }
            </div>
            </div>
        );
    }
}
