package database_handlers;

import java.io.IOException;

public class ActualDatabaseManager extends MySQLDatabaseManager {

	public ActualDatabaseManager(String propertyPath) throws IOException {
		super(propertyPath);
	}
	
	public ActualDatabaseManager() throws IOException {
		super();
	}
}
