import React from 'react';
import './login.css';
import axios from 'axios';
import Header from './Header';

export type JavaCities = {
    city_id: number,
    name: string,
    country_id: number
}

type CitiesState = {
    cities: JavaCities[],
}

export default class Cities extends React.Component<any, CitiesState> {

}