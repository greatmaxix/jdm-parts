package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.City;

public class CityTable extends ActualDatabaseManager  implements IBasicTable<City> {
	public CityTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public CityTable() throws IOException {
		super();
	}

	public String select(Integer car_model_id) {
		String query = "SELECT * FROM city WHERE city_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(car_model_id);
		List<Map<String, Object>> result = select(query, params);
		List<City> cities = new ArrayList<City>();
		for (int i = 0; i < result.size(); i++) {
			City tempCity = new City((Integer)result.get(i).get("city_id")
					,(String)result.get(i).get("name")
					,(Integer)result.get(i).get("country_id"));
			cities.add(tempCity);
		}
		String json = new Gson().toJson(cities.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM city";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<City> cities = new ArrayList<City>();
		for (int i = 0; i < result.size(); i++) {
			City tempCity = new City((Integer)result.get(i).get("city_id")
					,(String)result.get(i).get("name")
					,(Integer)result.get(i).get("country_id"));
			cities.add(tempCity);
		}
		String json = new Gson().toJson(cities);
		return json;
	}
	
	public void insert(City city) {
		String procedureName = "insert_city(?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(city.getName());
		params.add(city.getCountryId());
		runStoredProcedure(procedureName, params);
	}
	
	public void update(City city) {
		String procedureName = "update_city(?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(city.getId());
		params.add(city.getName());
		params.add(city.getCountryId());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer city_id) {
		String procedureName = "delete_city(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(city_id);
		runStoredProcedure(procedureName, params);
	}
}
