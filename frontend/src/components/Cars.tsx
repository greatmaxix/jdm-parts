import React from 'react';
import './login.css';
import axios from 'axios';
import Header from './Header';
import { JavaCarManufacturer } from './CarManufacturers';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';

export type JavaCar = {
    model_id?: number,
    name: string,
    car_man_id: number,
    car_man_date?: string,
}

type CarsState = {
    cars: JavaCar[],
    car_manufacturers: JavaCarManufacturer[],
    new_car: JavaCar,
    selected_car?: JavaCar
}

export default class Cars extends React.Component<any, CarsState> {
    constructor(props: any) {
        super(props);
        this.state = {
            cars: [],
            new_car: {
                name: "",
                car_man_id: -1,
            },
            car_manufacturers: []
        };
    }

    async reload() {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/carmodel',
        }).then((response: any) => {
            console.log(response);
            this.retrieveData(response.data);
        })
        .catch((error: any) => {
            console.log(error);
        });

        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/carmanufacturer',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.fillCarManufacturerList(response.data);
        })
            .catch((error: any) => {
                console.log(error);
            });
    }

    retrieveData(data: any) {
        let cars: JavaCar[] = data;
        this.setState({
            cars
        })
    }

    fillCarManufacturerList(data: any) {
        let car_manufacturers: JavaCarManufacturer[] = data;
        this.setState({
            car_manufacturers
        })
    }

    insertNewCar() {
        axios({
            method: 'put',
            url: 'http://127.0.0.1:8000/api/carmodel',
            headers: {},
            data: this.state.new_car
        }).then((response: any) => {
            console.log(response);
            window.location.reload(false);
        })
            .catch((error: any) => {
                console.log(error);
        });
    }

    updateCar() {
        if (this.state.selected_car) {
            axios({
                method: 'post',
                url: 'http://127.0.0.1:8000/api/carmodel',
                headers: {},
                data: this.state.selected_car
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
                .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    deleteCar() {
        if (this.state.selected_car) {
            axios({
                method: 'delete',
                url: 'http://127.0.0.1:8000/api/carmodel',
                headers: {},
                params: { modelid: + this.state.selected_car.model_id!}
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
            .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    componentWillMount() {
        console.log(localStorage.getItem("username"));
        if (!localStorage.getItem("username")) {
            this.props.history.push("/");
        }
        else {
            this.reload();
        }
    }

    handleInputChange(event: any) {
        let new_car: JavaCar = this.state.new_car;
        let selected_car: any = this.state.selected_car;

        if (selected_car) {
            if (event.target && event.target.name === 'updateCarName') {
                selected_car.name = event.target.value;
            }
            else if (event.target && event.target.name === 'updateCarManufacturer') {
                selected_car.car_man_id = event.target.value;
            }
            else {
                selected_car.car_man_date = moment(event).format('LL');
            }
        }
        else {
            if (event.target && event.target.name === 'newCarName') {
                new_car.name = event.target.value;
            }
            else if (event.target && event.target.name === 'newCarManufacturer') {
                new_car.car_man_id = event.target.value;
            }
            else {
                new_car.car_man_date = moment(event).format('LL');
            }
        }

        this.setState({
            new_car,
            selected_car
        })
    }

    onPartEditSelected(car_id: any) {
        if (car_id === undefined) {
            this.setState({
                selected_car: undefined
            })
            return;
        }

        let foundCar = this.state.cars.find((element: JavaCar) =>
            car_id === element.model_id
        );
        
        if (foundCar) {
            console.log((moment(foundCar.car_man_date).format('LL')))
            this.setState({
                selected_car: foundCar
            })
        }
    }

    render() {
        console.log("this.state", this.state);
        return (
            <div><Header currUrl="cars"></Header>
            <div className="container pt-5" >
                <div className="row login-row">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Model id</th>
                                <th scope="col">Car name</th>
                                <th scope="col">Car manufacturer id</th>
                                <th scope="col">Manufactured date</th>
                                <th scope="col"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartEditSelected.bind(this, undefined)}>Clear selection</button></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.cars.map((car: JavaCar, key: number) => {
                                return (
                                    <tr className={this.state.selected_car && this.state.selected_car.model_id === car.model_id ? "bg-info" : ""} key={car.model_id}>
                                        <th scope="row">{car.model_id}</th>
                                        <th>{car.name}</th>
                                        <th>{ (this.state.car_manufacturers.find((car_man: JavaCarManufacturer) => { if (car_man.car_man_id == car.car_man_id) return car_man.name }))?.name }</th>
                                        <th>{car.car_man_date}</th>
                                        <th scope="row"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartEditSelected.bind(this, car.model_id)}>Select to edit/delete</button></th>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    {this.state.selected_car ? 
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary" type="button" onClick={this.updateCar.bind(this)}>Update</button>
                                <button className="btn btn-outline-secondary" type="button" onClick={this.deleteCar.bind(this)}>Delete</button>
                            </div>
                            <input type="text" name="updateCarName" placeholder={this.state.selected_car.name} onChange={this.handleInputChange.bind(this)} className="form-control" />
                            <select className="custom-select" name="updateCarManufacturer" onChange={this.handleInputChange.bind(this)}>
                                {
                                    this.state.car_manufacturers.map((car_manufacturer: JavaCarManufacturer) => {
                                        if (car_manufacturer.car_man_id === this.state.selected_car!.car_man_id) {
                                            return (<option selected value={car_manufacturer.car_man_id}>{car_manufacturer.name}</option>)
                                        }
                                        else {
                                            return (
                                                <option value={car_manufacturer.car_man_id}>{car_manufacturer.name}</option>
                                            )
                                        }
                                    })
                                }
                            </select>
                            <DatePicker
                                className="form-control"
                                selected={ this.state.selected_car.car_man_date ?  new Date(moment(this.state.selected_car.car_man_date).format('LL')) : new Date(moment().format('LL')) }
                                onChange={ this.handleInputChange.bind(this) }
                                name="updateCarDate"
                                dateFormat="MM/d/yyyy"
                            />
                        </div>
                        : 
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary" type="button" onClick={this.insertNewCar.bind(this)}>Insert</button>
                            </div>
                            <input type="text" name="newCarName" placeholder="New car name" onChange={this.handleInputChange.bind(this)} className="form-control" />
                            <select className="custom-select" name="newCarManufacturer" onChange={this.handleInputChange.bind(this)}>
                                <option selected>New car`s manufacturer</option>
                                {    
                                    this.state.car_manufacturers.map((car_manufacturer: JavaCarManufacturer) => {
                                        return (
                                            <option value={car_manufacturer.car_man_id}>{car_manufacturer.name + " manufactured in: " + car_manufacturer.country_id}</option>
                                        )
                                    })
                                }
                            </select>
                            <DatePicker
                                className="form-control"
                                selected={ this.state.new_car.car_man_date ? new Date(moment(this.state.new_car.car_man_date).format('LL')) : new Date(moment().format('LL')) }
                                onChange={ this.handleInputChange.bind(this) }
                                name="updateCarDate"
                                dateFormat="MM/d/yyyy"
                            />
                        </div>
                    }
                </div>
            </div>
            </div>
        );
    }
}
