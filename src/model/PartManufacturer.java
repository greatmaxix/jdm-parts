package model;

public class PartManufacturer {
	private Integer id;
	private String name;
	private String desc;
	private Integer city_id;
	
	public PartManufacturer(Integer id, String name, String desc, Integer city_id) {
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.city_id = city_id;
	}
	
	public PartManufacturer(String name, String desc, Integer city_id) {
		this.name = name;
		this.desc = desc;
		this.city_id = city_id;
	}

	public Integer getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public Integer getCityId() {
		return this.city_id;
	}
}
