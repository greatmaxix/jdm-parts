package model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class User {
	private Integer user_id;
	private String username;
	private String password;
	private Integer city_id;
	
	public User(Integer user_id, String username, String password, Integer city_id) {
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.city_id = city_id;
	}
	
	public User(Integer user_id, String username, Integer city_id) {
		this.user_id = user_id;
		this.username = username;
		this.city_id = city_id;
	}
	
	public User(String username, String password, Integer city_id) {
		this.username = username;
		this.password = password;
		this.city_id = city_id;
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public User() {
	}

	public Integer getId() {
		return this.user_id;
	}
	
	public String getUserName() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public Integer getCityId() {
		return this.city_id;
	}
}
