package RequestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import model.City;
import table.CityTable;

public class CityController implements HttpHandler {

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		CityTable ctable = new CityTable();
		String respText;
		String noParams = "";
		Integer responseCode;
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		if (exchange.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
			exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE");
			exchange.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type,Authorization");
			exchange.sendResponseHeaders(204, -1);
            return;
        }
		//Read
		if ("GET".equals(exchange.getRequestMethod())) {
            Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
            String cityid = params.getOrDefault("cityid", List.of(noParams)).stream().findFirst().orElse(noParams);
            if (cityid.isEmpty()) {
            	respText = String.format(ctable.selectAll());
            }
            else {
            	respText = String.format(ctable.select(Integer.parseInt(cityid)));
            }
            responseCode = 200;
            exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
        }
		//Update
		else if ("POST".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
			InputStream is = exchange.getRequestBody();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				City cityObj = new Gson().fromJson(jsonBody, City.class);
				ctable.update(cityObj);
				respText = "Updated successfully!";
				responseCode = 200;
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		//Insert
		else if ("PUT".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
			InputStream is = exchange.getRequestBody();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				City cityObj = new Gson().fromJson(jsonBody, City.class);
				ctable.insert(cityObj);
				respText = "Inserted successfully!";
				responseCode = 200;
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		//Delete
		else if ("DELETE".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
            String cityid = params.getOrDefault("cityid", List.of(noParams)).stream().findFirst().orElse(noParams);
            try {
            	ctable.delete(Integer.parseInt(cityid));;
            	respText = "Deleted successfully!";
            	responseCode = 200;
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            	respText = "Error occured!";
            	responseCode = 420;
            }
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		else {
            exchange.sendResponseHeaders(405, -1);
        }
        exchange.close();
	}

}
