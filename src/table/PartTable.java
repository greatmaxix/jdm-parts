package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.Part;

public class PartTable extends ActualDatabaseManager implements IBasicTable<Part> {
	public PartTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public PartTable() throws IOException {
		super();
	}

	public String select(Integer part_id) {
		String query = "SELECT * FROM part WHERE part_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(part_id);
		List<Map<String, Object>> result = select(query, params);
		List<Part> parts = new ArrayList<Part>();
		for (int i = 0; i < result.size(); i++) {
			Part tempPart = new Part((Integer)result.get(i).get("part_id")
					,(String)result.get(i).get("name")
					,(String)result.get(i).get("desc")
					,(Integer)result.get(i).get("model_id")
					,(Integer)result.get(i).get("part_man_id"));
			parts.add(tempPart);
		}
		String json = new Gson().toJson(parts.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM part";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<Part> allParts = new ArrayList<Part>();
		for (int i = 0; i < result.size(); i++) {
			Part tempPart = new Part((Integer)result.get(i).get("part_id")
					,(String)result.get(i).get("name")
					,(String)result.get(i).get("desc")
					,(Integer)result.get(i).get("model_id")
					,(Integer)result.get(i).get("part_man_id"));
			allParts.add(tempPart);
		}
		String json = new Gson().toJson(allParts);
		return json;
	}
	
	public void insert(Part part) {
		String procedureName = "insert_part(?, ?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(part.getName());
		params.add(part.getDesc());
		params.add(part.getCarModelId());
		params.add(part.getPartManufacturerId());
		runStoredProcedure(procedureName, params);
	}
	
	public void update(Part part) {
		String procedureName = "update_part(?, ?, ?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(part.getId());
		params.add(part.getName());
		params.add(part.getDesc());
		params.add(part.getCarModelId());
		params.add(part.getPartManufacturerId());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer partid) {
		String procedureName = "delete_part(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(partid);
		runStoredProcedure(procedureName, params);
	}
}
