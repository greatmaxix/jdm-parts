package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.Warehouse;

public class WarehouseTable extends ActualDatabaseManager implements IBasicTable<Warehouse> {

	public WarehouseTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public WarehouseTable() throws IOException {
		super();
	}
	
	public String select(Integer id) {
		String query = "SELECT * FROM warehouse WHERE warehouse_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		List<Map<String, Object>> result = select(query, params);
		List<Warehouse> warehouses = new ArrayList<Warehouse>();
		for (int i = 0; i < result.size(); i++) {
			Warehouse tempWarehouse = new Warehouse((Integer)result.get(i).get("warehouse_id")
					,(String)result.get(i).get("name")
					,(Integer)result.get(i).get("city_id"));
			warehouses.add(tempWarehouse);
		}
		String json = new Gson().toJson(warehouses.get(0));
		return json;
	}

	public String selectAll() {
		String query = "SELECT * FROM warehouse";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<Warehouse> warehouses = new ArrayList<Warehouse>();
		for (int i = 0; i < result.size(); i++) {
			Warehouse tempWarehouse = new Warehouse((Integer)result.get(i).get("warehouse_id")
					,(String)result.get(i).get("name")
					,(Integer)result.get(i).get("city_id"));
			warehouses.add(tempWarehouse);
		}
		String json = new Gson().toJson(warehouses);
		return json;
	}

	public void insert(Warehouse w) {
		String procedureName = "insert_warehouse(?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(w.getName());
		params.add(w.getCityId());
		runStoredProcedure(procedureName, params);
	}

	public void update(Warehouse w) {
		String procedureName = "update_warehouse(?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(w.getId());
		params.add(w.getName());
		params.add(w.getCityId());
		runStoredProcedure(procedureName, params);
		
	}

	@Override
	public void delete(Integer id) {
		String procedureName = "delete_warehouse(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		runStoredProcedure(procedureName, params);
		
	}

}
