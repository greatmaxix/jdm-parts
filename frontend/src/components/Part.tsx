import React from 'react';
import './login.css';
import axios from 'axios';
import Header from './Header';
import { JavaCar } from './Cars';
import { JavaPartManufacturer } from './PartManufactureres';

export type JavaPart = {
    part_id?: number,
    name: string,
    desc: string,
    car_model_id: number,
    part_manufacturer_id: number
}

type PartState = {
    parts: JavaPart[],
    car_models: JavaCar[],
    part_manufacturers: JavaPartManufacturer[],
    new_part: JavaPart,
    selected_part?: JavaPart
}

export default class Part extends React.Component<any, PartState> {
    constructor(props: any) {
        super(props);
        this.state = {
            parts: [],
            car_models: [],
            part_manufacturers: [],
            new_part: {
                name: "",
                desc: "",
                car_model_id: -1,
                part_manufacturer_id: -1,
            }
        };
    }

    async reload() {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/parts',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.retrieveData(response.data);
        })
            .catch((error: any) => {
                console.log(error);
            });

        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/carmodel',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.fillCarModelsList(response.data);
        })
            .catch((error: any) => {
                console.log(error);
            });

        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/partmanufacturer',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.fillPartManufacturersList(response.data);
        })
            .catch((error: any) => {
                console.log(error);
            });
    }

    async insert() {

    }

    async update() {

    }

    async delete() {

    }

    retrieveData(data: any) {
        let parts: JavaPart[] = data;
        this.setState({
            parts
        })
    }

    fillCarModelsList(data: any) {
        let car_models: JavaCar[] = data;
        this.setState({
            car_models,
        })
    }

    fillPartManufacturersList(data: any) {
        let part_manufacturers: JavaPartManufacturer[] = data;
        this.setState({
            part_manufacturers,
        })
    }

    insertNewPart() {
        axios({
            method: 'put',
            url: 'http://127.0.0.1:8000/api/part',
            headers: {},
            data: this.state.new_part
        }).then((response: any) => {
            console.log(response);
            window.location.reload(false);
        })
            .catch((error: any) => {
                console.log(error);
        });
    }

    updatePart() {
        if (this.state.selected_part) {
            axios({
                method: 'post',
                url: 'http://127.0.0.1:8000/api/part',
                headers: {},
                data: this.state.selected_part
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
                .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    deletePart() {
        if (this.state.selected_part) {
            axios({
                method: 'delete',
                url: 'http://127.0.0.1:8000/api/part',
                headers: {},
                params: { partid: + this.state.selected_part.part_id!}
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
            .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    componentWillMount() {
        console.log(localStorage.getItem("username"));
        if (!localStorage.getItem("username")) {
            this.props.history.push("/");
        }
        else {
            this.reload();
        }
    }

    handleInputChange(event: any) {
        let new_part: JavaPart = this.state.new_part;
        let selected_part: any = this.state.selected_part;
        if (event.target && event.target.name === 'newPartName') {
            new_part.name = event.target.value;
        }
        else if (event.target && event.target.name === 'newPartDesc') {
            new_part.desc = event.target.value;
        }
        else if (event.target && event.target.name === 'newPartModel') {
            new_part.car_model_id = event.target.value;
        }
        else if (event.target && event.target.name === 'newPartPartMan') {
            new_part.part_manufacturer_id = event.target.value;
        }

        if (selected_part) {
            if (event.target && event.target.name === 'updatePartName') {
                selected_part.name = event.target.value;
            }
            else if (event.target && event.target.name === 'updatePartDesc') {
                selected_part.desc = event.target.value;
            }
            else if (event.target && event.target.name === 'updatePartModel') {
                selected_part.car_model_id = event.target.value;
            }
            else if (event.target && event.target.name === 'updatePartPartMan') {
                selected_part.part_manufacturer_id = event.target.value;
            }
        }

        this.setState({
            new_part,
            selected_part
        })
    }

    onPartEditSelected(part_id: any) {
        if (part_id === undefined) {
            this.setState({
                selected_part: undefined
            })
            return;
        }

        let foundPart = this.state.parts.find((element: JavaPart) =>
            part_id === element.part_id
        );
        if (foundPart) {
            this.setState({
                selected_part: foundPart
            })
        }
    }

    render() {
        return (
            <div><Header currUrl="parts"></Header>
                <div className="container pt-5" >
                    <div className="row login-row">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">Part id</th>
                                    <th scope="col">Part name</th>
                                    <th scope="col">Part description</th>
                                    <th scope="col">Car model</th>
                                    <th scope="col">Part manufacturer</th>
                                    <th scope="col"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartEditSelected.bind(this, undefined)}>Clear selection</button></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.parts.map((part: JavaPart) => {
                                    return (
                                        <tr className={this.state.selected_part && this.state.selected_part.part_id === part.part_id ? "bg-info" : ""}>
                                            <th scope="row">{part.part_id}</th>
                                            <th>{part.name}</th>
                                            <th>{part.desc}</th>
                                            <th>{(this.state.car_models.find((el)=> {return el.model_id == part.car_model_id}))?.name}</th>
                                            <th>{(this.state.part_manufacturers.find((el)=>{return el.id == part.part_manufacturer_id}))?.name}</th>
                                            <th scope="row"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartEditSelected.bind(this, part.part_id)}>Select to edit/delete</button></th>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                    {this.state.selected_part ?
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary" type="button" onClick={this.updatePart.bind(this)}>Update</button>
                                <button className="btn btn-outline-secondary" type="button" onClick={this.deletePart.bind(this)}>Delete</button>
                            </div>
                            <input type="text" name="updatePartName" placeholder={this.state.selected_part.name} onChange={this.handleInputChange.bind(this)} className="form-control" />
                            <input type="text" name="updatePartDesc" placeholder={this.state.selected_part.desc} onChange={this.handleInputChange.bind(this)} className="form-control" />
                            <select className="custom-select" name="updatePartModel" onChange={this.handleInputChange.bind(this)}>
                                {
                                    this.state.car_models.map((model: JavaCar) => {
                                        if (model.model_id === this.state.selected_part!.car_model_id) {
                                            return (<option selected value={model.model_id}>{model.name + " manufactured on: " + model.car_man_date}</option>)
                                        }
                                        else {
                                            return (
                                                <option value={model.model_id}>{model.name + " manufactured on: " + model.car_man_date}</option>
                                            )
                                        }
                                    })
                                }
                            </select>
                            <select className="custom-select" name="updatePartPartMan" onChange={this.handleInputChange.bind(this)}>
                                {
                                    this.state.part_manufacturers.map((part: JavaPartManufacturer) => {
                                        if (part.id === this.state.selected_part!.part_manufacturer_id) {
                                            return <option selected value={part.id}>{part.name}</option>
                                        }
                                        else {
                                            return (
                                                <option value={part.id}>{part.name}</option>
                                            )
                                        }
                                    })
                                }
                            </select>
                        </div>
                        :
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <button className="btn btn-outline-secondary" type="button" onClick={this.insertNewPart.bind(this)}>Insert</button>
                            </div>
                            <input type="text" name="newPartName" placeholder="New part name" onChange={this.handleInputChange.bind(this)} className="form-control" />
                            <input type="text" name="newPartDesc" placeholder="New part description" onChange={this.handleInputChange.bind(this)} className="form-control" />
                            <select className="custom-select" name="newPartModel" onChange={this.handleInputChange.bind(this)}>
                                <option selected>New part`s car model</option>
                                {
                                    this.state.car_models.map((model: JavaCar) => {
                                        return (
                                            <option value={model.model_id}>{model.name + " manufactured on: " + model.car_man_date}</option>
                                        )
                                    })
                                }
                            </select>
                            <select className="custom-select" name="newPartPartMan" onChange={this.handleInputChange.bind(this)}>
                                <option selected>New part`s manufacturer</option>
                                {
                                    this.state.part_manufacturers.map((part: JavaPartManufacturer) => {
                                        return (
                                            <option value={part.id}>{part.name}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                    }
                </div>
            </div>
        );
    }
}
