package table;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.User;

public class UserTable extends ActualDatabaseManager implements IBasicTable<User> {

	public UserTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public UserTable() throws IOException {
		super();
	}

	public String select(Integer user_id) {
		String query = "SELECT * FROM users WHERE part_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(user_id);
		List<Map<String, Object>> result = select(query, params);
		List<User> users = new ArrayList<User>();
		for (int i = 0; i < result.size(); i++) {
			User tempUser = new User((Integer)result.get(i).get("user_id")
					,(String)result.get(i).get("username")
					,(Integer)result.get(i).get("city_id"));
			users.add(tempUser);
		}
		String json = new Gson().toJson(users.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM users";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<User> users = new ArrayList<User>();
		for (int i = 0; i < result.size(); i++) {
			User tempPart = new User((Integer)result.get(i).get("user_id")
					,(String)result.get(i).get("username")
					,(Integer)result.get(i).get("city_id"));
			users.add(tempPart);
		}
		String json = new Gson().toJson(users);
		return json;
	}
	
	public boolean selectByUsernameAndPwd(String username, String password) {
		String query = "SELECT * FROM users WHERE username = ? AND password = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(username);
		params.add(password);
		return checkSelect(query, params);
	}
	
	public void insert(User user) {
		String procedureName = "insert_user(?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(user.getUserName());
		params.add(getHashedPwd(user.getPassword()));
		params.add(user.getCityId());
		runStoredProcedure(procedureName, params);
	}
	
	public static String getHashedPwd(String pwd) {
		String sha1 = "";
	    try
	    {
	        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
	        crypt.reset();
	        crypt.update(pwd.getBytes("UTF-8"));
	        sha1 = byteToHex(crypt.digest());
	    }
	    catch(NoSuchAlgorithmException e)
	    {
	        e.printStackTrace();
	    }
	    catch(UnsupportedEncodingException e)
	    {
	        e.printStackTrace();
	    }
	    return sha1;
	}
	
	private static String byteToHex(final byte[] hash)
	{
	    Formatter formatter = new Formatter();
	    for (byte b : hash)
	    {
	        formatter.format("%02x", b);
	    }
	    String result = formatter.toString();
	    formatter.close();
	    return result;
	}
	
	public void update(User user) {
		String procedureName = "update_user(?, ?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(user.getId());
		params.add(user.getUserName());
		params.add(getHashedPwd(user.getPassword()));
		params.add(user.getCityId());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer userid) {
		String procedureName = "delete_user(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(userid);
		runStoredProcedure(procedureName, params);
	}
}
