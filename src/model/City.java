package model;

public class City {
	private Integer city_id;
	private String name;
	private Integer country_id;
	
	public City(Integer city_id, String name, Integer country_id) {
		this.city_id = city_id;
		this.name = name;
		this.country_id = country_id;
	}
	
	public City(String name, Integer country_id) {
		this.name = name;
		this.country_id = country_id;
	}

	public Integer getId() {
		return this.city_id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Integer getCountryId() {
		return this.country_id;
	}
}
