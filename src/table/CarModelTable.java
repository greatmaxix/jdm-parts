package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.sql.Date;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.CarModel;

public class CarModelTable extends ActualDatabaseManager implements IBasicTable<CarModel> {
	public CarModelTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public CarModelTable() throws IOException {
		super();
	}

	public String select(Integer car_model_id) {
		String query = "SELECT * FROM car_model WHERE model_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(car_model_id);
		List<Map<String, Object>> result = select(query, params);
		List<CarModel> models = new ArrayList<CarModel>();
		for (int i = 0; i < result.size(); i++) {
			CarModel tempModel = new CarModel((Integer)result.get(i).get("model_id")
					,(String)result.get(i).get("name")
					,(Integer)result.get(i).get("car_man_id")
					,(Date)result.get(i).get("manufactured_date"));
			models.add(tempModel);
		}
		String json = new Gson().toJson(models.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM car_model";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<CarModel> models = new ArrayList<CarModel>();
		for (int i = 0; i < result.size(); i++) {
			CarModel tempModel = new CarModel((Integer)result.get(i).get("model_id")
					,(String)result.get(i).get("name")
					,(Integer)result.get(i).get("car_man_id")
					,(Date)result.get(i).get("manufactured_date"));
			models.add(tempModel);
		}
		String json = new Gson().toJson(models);
		return json;
	}
	
	public void insert(CarModel car_model) {
		String procedureName = "insert_car_model(?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(car_model.getName());
		params.add(car_model.getCarManId());
		params.add(car_model.getCarManDate());
		runStoredProcedure(procedureName, params);
	}
	
	public void update(CarModel car_model) {
		String procedureName = "update_car_model(?, ?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(car_model.getId());
		params.add(car_model.getName());
		params.add(car_model.getCarManId());
		params.add(car_model.getCarManDate());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer car_model_id) {
		String procedureName = "delete_car_model(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(car_model_id);
		runStoredProcedure(procedureName, params);
	}
}
