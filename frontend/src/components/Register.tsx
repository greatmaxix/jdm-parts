import React from 'react';
import './login.css';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

type RegisterState = {
    username: string,
    password: string
}



export default class Register extends React.Component<any, RegisterState> {    
    constructor(props: any) {
        super(props);
        this.state = {
            username: "",
            password: "",
        }
    }
    

    async onLoginClick() {
        console.log("Login clicked!");
        let apiBaseUrl = "http://localhost:8000/api/";
        let payload = {
            "username": this.state.username,
            "password": this.state.password
        }

        axios.post(apiBaseUrl + 'login', payload)
            .then((response: any) => {
                console.log(response);
                if (response.data.code == 200) {
                    console.log("Login successfull");

                }
                else if (response.data.code == 204) {
                    console.log("Username password do not match");
                    alert("username password do not match")
                }
                else {
                    console.log("Username does not exists");
                    alert("Username does not exist");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handleInputChange(event: any) {
        let username = this.state.username;
        let password = this.state.password;
        if (event.target && event.target.name === 'username') {
            username = event.target.value;
        }
        else if (event.target && event.target.name === 'password') {
            password = event.target.value;
        }
        this.setState({
            username,
            password
        })
    }

    loginClicked() {
        this.props.history.push("/");
    }

    render() {
        return (
            <div className="container pt-5" >
                <div className="row login-row">
                    <div className="card login-center">
                        <article className="card-body">
                            <a onClick={this.loginClicked.bind(this)} className="float-right btn btn-outline-primary">Sign in</a>
                            <h4 className="card-title mb-4 mt-1">Register</h4>
                            <form method="POST" action="/login">
                                <div className="form-group">
                                    <label>Your username</label>
                                    <input name="username" className="form-control" placeholder="Username" type="username" onChange={this.handleInputChange.bind(this)} />
                                </div>
                                <div className="form-group">
                                    <label>Your password</label>
                                    <input name="password" className="form-control" placeholder="******" type="password" onChange={this.handleInputChange.bind(this)}/>
                                </div>
                                <div className="form-group">
                                    <button type="submit" className="btn btn-primary btn-block" onClick={this.onLoginClick.bind(this)}>Register</button>
                                </div>
                            </form>
                        </article>
                    </div>
                </div>
            </div>
        );
    }
}
