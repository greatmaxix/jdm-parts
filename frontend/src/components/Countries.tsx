import React from 'react';
import './login.css';
import axios from 'axios';
import Header from './Header';

export type JavaCountries = {
    country_id: number,
    name: string,
}

type CountriesState = {
    countries: JavaCountries[],
}

export default class Countries extends React.Component<any, CountriesState> {

}