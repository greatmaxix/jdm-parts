package RequestController;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

public class MainRequestController {
	public void startServer() {
		try {
			Integer serverPort = 8000;
			HttpServer server = HttpServer.create(new InetSocketAddress(serverPort), 0);
			HttpContext loginContext = server.createContext("/api/login", new UserController());
			HttpContext partContext = server.createContext("/api/part", new PartController());
			HttpContext partManContext = server.createContext("/api/partmanufacturer", new PartManufacturerController());
			HttpContext carManContext = server.createContext("/api/carmanufacturer", new CarManufacturerController());
			HttpContext carModelContext = server.createContext("/api/carmodel", new CarModelController());
			HttpContext warehouseContext = server.createContext("/api/warehouse", new WarehouseController());
			HttpContext cityContext = server.createContext("/api/city", new CityController());
			HttpContext countryContext = server.createContext("/api/country", new CountryController());
			
			List<HttpContext> protectedContextList = new ArrayList<HttpContext>();
			protectedContextList.add(partContext);
			protectedContextList.add(partManContext);
			protectedContextList.add(carManContext);
			protectedContextList.add(carModelContext);
			protectedContextList.add(warehouseContext);
			protectedContextList.add(cityContext);
			protectedContextList.add(countryContext);
			
			//this one works, but idk how to handle it on frontend
			/*for (int i = 0; i < protectedContextList.size(); i++) {
				protectedContextList.get(i).setAuthenticator(new BasicAuthenticator("maincontentrealm") {
			        @Override
			        public boolean checkCredentials(String username, String pwd) {
			        	return UserController.checkIfExists(username, pwd);
			        }
			    });
			}*/
			
	    	server.setExecutor(null);
	    	server.start();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
