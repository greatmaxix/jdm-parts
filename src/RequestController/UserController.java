package RequestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import model.Part;
import model.User;
import table.UserTable;

class UserController implements HttpHandler {
	public static boolean checkIfExists(String username, String sha1password) {
		boolean userExists = false;
		try {
			UserTable utable = new UserTable();
			userExists = utable.selectByUsernameAndPwd(username, UserTable.getHashedPwd(sha1password));
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return userExists;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		String respText = "";
		Integer responseCode = 420;
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		if (exchange.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
			exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE");
			exchange.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type,Authorization");
			exchange.sendResponseHeaders(204, -1);
            return;
        }
		if ("POST".equals(exchange.getRequestMethod())) {
            InputStream is = exchange.getRequestBody();
            User user = new User();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				user = new Gson().fromJson(jsonBody, User.class);
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
            String username = user.getUserName();
            String password = user.getPassword();
            boolean userExists = !username.equals("") && !password.equals("") && UserController.checkIfExists(username, password); 
            if (userExists) {
            	respText = String.format("Success!");
            	System.out.println("uspwd: "+ username + password);
            	responseCode = 200;
            	exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            }
            else {
            	respText = String.format("User not found!");
            	responseCode = 420;
            	exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            }
            
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
        } else {
            exchange.sendResponseHeaders(405, -1);
        }
        exchange.close();
	}
}
