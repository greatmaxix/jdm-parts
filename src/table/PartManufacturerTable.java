package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.PartManufacturer;

public class PartManufacturerTable extends ActualDatabaseManager implements IBasicTable<PartManufacturer> {
	public PartManufacturerTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public PartManufacturerTable() throws IOException {
		super();
	}

	public String select(Integer part_man_id) {
		String query = "SELECT * FROM part_manufacturer WHERE id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(part_man_id);
		List<Map<String, Object>> result = select(query, params);
		List<PartManufacturer> partMans = new ArrayList<PartManufacturer>();
		for (int i = 0; i < result.size(); i++) {
			PartManufacturer tempPartMan = new PartManufacturer((Integer)result.get(i).get("id")
					,(String)result.get(i).get("name")
					,(String)result.get(i).get("description")
					,(Integer)result.get(i).get("city_id"));
			partMans.add(tempPartMan);
		}
		System.out.println("partman:" + partMans.get(0));
		String json = new Gson().toJson(partMans.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM part_manufacturer";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<PartManufacturer> partMans = new ArrayList<PartManufacturer>();
		for (int i = 0; i < result.size(); i++) {
			PartManufacturer tempPartMan = new PartManufacturer((Integer)result.get(i).get("id")
					,(String)result.get(i).get("name")
					,(String)result.get(i).get("description")
					,(Integer)result.get(i).get("city_id"));
			partMans.add(tempPartMan);
		}
		String json = new Gson().toJson(partMans);
		return json;
	}
	
	public void insert(PartManufacturer part) {
		String procedureName = "insert_part_manufacturer(?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(part.getName());
		params.add(part.getDesc());
		params.add(part.getCityId());
		runStoredProcedure(procedureName, params);
	}
	
	public void update(PartManufacturer part) {
		String procedureName = "update_part_manufacturer(?, ?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(part.getId());
		params.add(part.getName());
		params.add(part.getDesc());
		params.add(part.getCityId());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer part_id) {
		String procedureName = "delete_part_manufacturer(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(part_id);
		runStoredProcedure(procedureName, params);
	}
}
