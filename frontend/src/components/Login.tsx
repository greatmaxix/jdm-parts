import React from 'react';
import './login.css';
import axios from 'axios';

type LoginState = {
    username: string,
    password: string
}

export default class Login extends React.Component<any, LoginState> {
    constructor(props: any) {
        super(props);
        this.state = {
            username: "",
            password: "",
        }
    }

    async onLoginClick() {
        axios({
            method: 'post',
            url: 'http://127.0.0.1:8000/api/login',
            headers: {},
            data: {
                username: this.state.username,
                password: this.state.password,
            }
        }).then((response:any) => {
            console.log(response);
            localStorage.setItem("username", this.state.username);
            console.log("Login successfull");
            this.props.history.push("/parts");
        })
        .catch((error: any) => {
            console.log(error);
        });
    }

    handleInputChange(event: any) {
        let username = this.state.username;
        let password = this.state.password;
        if (event.target && event.target.name === 'username') {
            username = event.target.value;
        }
        else if (event.target && event.target.name === 'password') {
            password = event.target.value;
        }
        this.setState({
            username,
            password
        })
    }

    registerClicked() {
        this.props.history.push("/register");
    }

    render() {
        return (
            <div className="container pt-5" >
                <div className="row login-row">
                    <div className="card login-center">
                        <article className="card-body">
                            <a onClick={this.registerClicked.bind(this)} className="float-right btn btn-outline-primary">Register</a>
                            <h4 className="card-title mb-4 mt-1">Sign in</h4>
                            <div className="form-group">
                                <label>Your username</label>
                                <input name="username" className="form-control" placeholder="Username" type="username" onChange={this.handleInputChange.bind(this)} />
                            </div>
                            <div className="form-group">
                                <label>Your password</label>
                                <input name="password" className="form-control" placeholder="******" type="password" onChange={this.handleInputChange.bind(this)}/>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary btn-block" onClick={this.onLoginClick.bind(this)}>Login</button>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        );
    }
}
