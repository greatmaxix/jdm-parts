package table;

interface IBasicTable<T> {
	public String select(Integer id);
	public String selectAll();
	public void insert(T t);
	public void update(T t);
	public void delete(Integer id);
}
