package model;

public class Part {
	private Integer part_id;
	private String name;
	private String desc;
	private Integer car_model_id;
	private Integer part_manufacturer_id;
	
	public Part(Integer part_id, String name, String desc, Integer car_model_id, Integer part_manufacturer_id) {
		this.part_id = part_id;
		this.name = name;
		this.desc = desc;
		this.car_model_id = car_model_id;
		this.part_manufacturer_id = part_manufacturer_id;
	}
	
	public Part(String name, String desc, Integer car_model_id, Integer part_manufacturer_id) {
		this.name = name;
		this.desc = desc;
		this.car_model_id = car_model_id;
		this.part_manufacturer_id = part_manufacturer_id;
	}

	public Integer getId() {
		return this.part_id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public Integer getCarModelId() {
		return this.car_model_id;
	}
	
	public Integer getPartManufacturerId() {
		return this.part_manufacturer_id;
	}
}
