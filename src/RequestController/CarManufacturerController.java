package RequestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import model.CarManufacturer;
import table.CarManufacturerTable;

public class CarManufacturerController implements HttpHandler {

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		CarManufacturerTable cmantable = new CarManufacturerTable();
		String respText;
		String noParams = "";
		Integer responseCode;
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		if (exchange.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
			exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE");
			exchange.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type,Authorization");
			exchange.sendResponseHeaders(204, -1);
            return;
        }
		//Read
		if ("GET".equals(exchange.getRequestMethod())) {
            Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
            String manid = params.getOrDefault("carid", List.of(noParams)).stream().findFirst().orElse(noParams);
            if (manid.isEmpty()) {
            	respText = String.format(cmantable.selectAll());
            }
            else {
            	respText = String.format(cmantable.select(Integer.parseInt(manid)));
            }
            responseCode = 200;
            exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
        }
		//Update
		else if ("POST".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
			InputStream is = exchange.getRequestBody();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				CarManufacturer partManObj = new Gson().fromJson(jsonBody, CarManufacturer.class);
				cmantable.update(partManObj);
				respText = "Updated successfully!";
				responseCode = 200;
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		//Insert
		else if ("PUT".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
			InputStream is = exchange.getRequestBody();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				CarManufacturer partManObj = new Gson().fromJson(jsonBody, CarManufacturer.class);
				cmantable.insert(partManObj);
				respText = "Inserted successfully!";
				responseCode = 200;
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		//Delete
		else if ("DELETE".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
            String partmanid = params.getOrDefault("carid", List.of(noParams)).stream().findFirst().orElse(noParams);
            try {
            	cmantable.delete(Integer.parseInt(partmanid));;
            	respText = "Deleted successfully!";
            	responseCode = 200;
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            	respText = "Error occured!";
            	responseCode = 420;
            }
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		else {
            exchange.sendResponseHeaders(405, -1);
        }
        exchange.close();
	}

}
