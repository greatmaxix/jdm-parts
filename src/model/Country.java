package model;

public class Country {
	private Integer country_id;
	private String name;
	
	public Country(Integer country_id, String name) {
		this.country_id =country_id;
		this.name = name;
		this.country_id = country_id;
	}
	
	public Country(String name) {
		this.name = name;
	}

	public Integer getId() {
		return this.country_id;
	}
	
	public String getName() {
		return this.name;
	}
}
