import React from 'react';
import './login.css';
import axios from 'axios';
import Header from './Header';
import { JavaCountries } from './Countries';

export type JavaCarManufacturer = {
    car_man_id?: number,
    name: string,
    country_id: number
}

type CarManufacturerState = {
    carmans: JavaCarManufacturer[],
    new_carman: JavaCarManufacturer,
    selected_carman?: any,
    countries: JavaCountries[],
}

export default class CarManufacturer extends React.Component<any, CarManufacturerState> {
    constructor(props: any) {
        super(props);
        this.state = {
            carmans: [],
            countries: [],
            new_carman: {
                name: "",
                country_id: -1,
            }
        };
    }

    async reload() {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/carmanufacturer',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.retrieveData(response.data);
        })
        .catch((error: any) => {
            console.log(error);
        });

        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/api/country',
            headers: {},
            data: {
            }
        }).then((response: any) => {
            this.fillCountriesList(response.data);
        })
            .catch((error: any) => {
                console.log(error);
            });
    }

    retrieveData(data: any) {
        let carmans: JavaCarManufacturer[] = data;
        this.setState({
            carmans
        })
    }

    fillCountriesList(data: any) {
        let countries: JavaCountries[] = data;
        this.setState({
            countries,
        })
    }

    insertNewCarMan() {
        axios({
            method: 'put',
            url: 'http://127.0.0.1:8000/api/carmanufacturer',
            headers: {},
            data: this.state.new_carman
        }).then((response: any) => {
            console.log(response);
            window.location.reload(false);
        })
            .catch((error: any) => {
                console.log(error);
        });
    }

    updateCarMan() {
        if (this.state.selected_carman) {
            axios({
                method: 'post',
                url: 'http://127.0.0.1:8000/api/carmanufacturer',
                headers: {},
                data: this.state.selected_carman
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
                .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    deleteCarMan() {
        if (this.state.selected_carman) {
            axios({
                method: 'delete',
                url: 'http://127.0.0.1:8000/api/carmanufacturer',
                headers: {},
                params: { carid: + this.state.selected_carman.car_man_id!}
            }).then((response: any) => {
                console.log(response);
                window.location.reload(false);
            })
            .catch((error: any) => {
                    console.log(error);
            });
        }
    }

    componentWillMount() {
        console.log(localStorage.getItem("username"));
        if (!localStorage.getItem("username")) {
            this.props.history.push("/");
        }
        else {
            this.reload();
        }
    }

    handleInputChange(event: any) {
        let new_carman: JavaCarManufacturer = this.state.new_carman;
        let selected_carman: any = this.state.selected_carman;
        if (event.target && event.target.name === 'newCarManName') {
            new_carman.name = event.target.value;
        }
        else if (event.target && event.target.name === 'newCarManCountry') {
            new_carman.country_id = event.target.value;
        }

        if (selected_carman) {
            if (event.target && event.target.name === 'updateCarManName') {
                selected_carman.name = event.target.value;
            }
            else if (event.target && event.target.name === 'updateCarManCountry') {
                selected_carman.country_id = event.target.value;
            }
        }
        this.setState({
            new_carman,
            selected_carman
        })
    }

    onPartManEditSelected(car_man_id: any) {
        if (car_man_id === undefined) {
            this.setState({
                selected_carman: undefined
            })
            return;
        }

        let foundCarMan = this.state.carmans.find((element: JavaCarManufacturer) =>
            car_man_id === element.car_man_id
        );
        if (foundCarMan) {
            this.setState({
                selected_carman: foundCarMan
            })
        }
    }

    render() {
        console.log("this.state", this.state);
        return (
            <div><Header currUrl="carman"></Header>
            <div className="container pt-5" >
                <div className="row login-row">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Car manufacturer id</th>
                                <th scope="col">Part manufacturer name</th>
                                <th scope="col">Country</th>
                                <th scope="col"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartManEditSelected.bind(this, undefined)}>Clear selection</button></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.carmans.map((carman: JavaCarManufacturer) => {
                                return (
                                    <tr className={this.state.selected_carman && this.state.selected_carman.car_man_id === carman.car_man_id ? "bg-info" : ""}>
                                        <th scope="row">{carman.car_man_id}</th>
                                        <th>{carman.name}</th>
                                        <th>{(this.state.countries.find((el)=>{return el.country_id == carman.country_id}))?.name}</th>
                                        <th scope="row"><button className="btn btn-outline-secondary btn-sm" type="button" onClick={this.onPartManEditSelected.bind(this, carman.car_man_id)}>Select to edit/delete</button></th>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                {this.state.selected_carman ? 
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <button className="btn btn-outline-secondary" type="button" onClick={this.updateCarMan.bind(this)}>Update</button>
                            <button className="btn btn-outline-secondary" type="button" onClick={this.deleteCarMan.bind(this)}>Delete</button>
                        </div>
                        <input type="text" name="updateCarManName" placeholder={this.state.selected_carman.name} onChange={this.handleInputChange.bind(this)} className="form-control" />
                        <select className="custom-select" name="updateCarManCountry" onChange={this.handleInputChange.bind(this)}>
                                {
                                    this.state.countries.map((country: JavaCountries) => {
                                        if (country.country_id === this.state.selected_carman!.country_id) {
                                            return (<option selected value={country.country_id}>{country.name}</option>)
                                        }
                                        else {
                                            return (
                                                <option value={country.country_id}>{country.name}</option>
                                            )
                                        }
                                    })
                                }
                            </select>
                    </div>
                    :
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <button className="btn btn-outline-secondary" type="button" onClick={this.insertNewCarMan.bind(this)}>Insert</button>
                        </div>
                        <input type="text" name="newCarManName" placeholder="New car manufacturer name" onChange={this.handleInputChange.bind(this)} className="form-control" />
                        <select className="custom-select" name="newCarManCountry" onChange={this.handleInputChange.bind(this)}>
                            <option selected>New car manufacturer`s country</option>
                                {
                                    this.state.countries.map((country: JavaCountries) => {
                                        return (
                                            <option value={country.country_id}>{country.name}</option>
                                        )
                                    })
                                }
                            </select>
                    </div>
                }
            </div>
            </div>
        );
    }
}