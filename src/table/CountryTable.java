package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.Country;

public class CountryTable extends ActualDatabaseManager implements IBasicTable<Country> {
	public CountryTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public CountryTable() throws IOException {
		super();
	}

	public String select(Integer country_id) {
		String query = "SELECT * FROM country WHERE country_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(country_id);
		List<Map<String, Object>> result = select(query, params);
		List<Country> countries = new ArrayList<Country>();
		for (int i = 0; i < result.size(); i++) {
			Country tempCountry = new Country((Integer)result.get(i).get("country_id")
					,(String)result.get(i).get("name"));
			countries.add(tempCountry);
		}
		String json = new Gson().toJson(countries.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM country";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<Country> countries = new ArrayList<Country>();
		for (int i = 0; i < result.size(); i++) {
			Country tempCountry = new Country((Integer)result.get(i).get("country_id")
					,(String)result.get(i).get("name"));
			countries.add(tempCountry);
		}
		String json = new Gson().toJson(countries);
		return json;
	}
	
	public void insert(Country country) {
		String procedureName = "insert_country(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(country.getName());
		runStoredProcedure(procedureName, params);
	}
	
	public void update(Country country) {
		String procedureName = "update_country(?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(country.getId());
		params.add(country.getName());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer country_id) {
		String procedureName = "delete_country(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(country_id);
		runStoredProcedure(procedureName, params);
	}
}
