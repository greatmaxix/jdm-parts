import React from 'react';
import './App.css';
import Login from './components/Login';
import Register from './components/Register';
import Part from './components/Part';
import Cars from './components/Cars';
import PartManufacturer from './components/PartManufactureres';
import CarManufacturer from './components/CarManufacturers';
import MainPage from './components/MainPage';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";



function App() {
  return (
    <Router>
        <Route exact path="/" component={Login}/>
        <Route exact path="/mainpage" component={MainPage}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/parts" component={Part}/>
        <Route exact path="/cars" component={Cars}/>
        <Route exact path="/partman" component={PartManufacturer}/>
        <Route exact path="/carman" component={CarManufacturer}/>
    </Router>
  );
}

export default App;
