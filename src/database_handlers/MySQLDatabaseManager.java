package database_handlers;

import java.io.IOException;

public class MySQLDatabaseManager extends BaseDatabaseManager {
	
	public MySQLDatabaseManager(String propertyPath) throws IOException {
		super(propertyPath);
	}
	
	public MySQLDatabaseManager() throws IOException {
		super();
	}

	// URL: jdbc://localhost/DB_NAME
	@Override
	protected String getDatabaseUrl() {
		StringBuilder builder = new StringBuilder();

		builder.append("jdbc:mysql://");
		builder.append(this.getHost());
		builder.append("/");
		builder.append(this.getName());
		builder.append("?");
		builder.append("serverTimezone=");
		builder.append(this.getTimeZone());

		return builder.toString();
		
	}
}
