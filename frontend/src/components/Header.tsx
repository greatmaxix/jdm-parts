import React from 'react';
import './login.css';

export type HeaderProps = {
    currUrl: string
}

export default class Header extends React.Component<HeaderProps, any> {
    constructor(props: HeaderProps) {
        super(props);
    }

    logOut() {
        localStorage.clear();
        window.location.reload();
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light mb-auto">
                <a href="/mainpage" className="navbar-brand">JDM parts</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className={this.props.currUrl === "parts" ? "nav-item active" : "nav-item"} >
                            <a className="nav-link" href="/parts">Parts</a>
                        </li>
                        <li className={this.props.currUrl === "cars" ? "nav-item active" : "nav-item"}>
                            <a className="nav-link" href="/cars">Cars</a>
                        </li>
                        <li className={this.props.currUrl === "partman" ? "nav-item active" : "nav-item"}>
                            <a className="nav-link" href="/partman">Part manufacturers</a>
                        </li>
                        <li className={this.props.currUrl === "carman" ? "nav-item active" : "nav-item"}>
                            <a className="nav-link" href="/carman">Car manufacturers</a>
                        </li>
                    </ul>
                    
                </div>
                <button className="btn btn-outline-warning my-2 my-sm-0" onClick={this.logOut.bind(this)}>Log out</button>
            </nav>
        );
    }
}
