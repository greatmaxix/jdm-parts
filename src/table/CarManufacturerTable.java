package table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import database_handlers.ActualDatabaseManager;
import model.CarManufacturer;

public class CarManufacturerTable extends ActualDatabaseManager implements IBasicTable<CarManufacturer> {
	public CarManufacturerTable(String propertyPath) throws IOException {
		super(propertyPath);
		// TODO Auto-generated constructor stub
	}
	
	public CarManufacturerTable() throws IOException {
		super();
	}

	public String select(Integer car_id) {
		String query = "SELECT * FROM car_manufacturer WHERE car_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(car_id);
		List<Map<String, Object>> result = select(query, params);
		List<CarManufacturer> cars = new ArrayList<CarManufacturer>();
		for (int i = 0; i < result.size(); i++) {
			CarManufacturer tempCar = new CarManufacturer((Integer)result.get(i).get("car_id")
					,(String)result.get(i).get("name")
					,(String)result.get(i).get("desc")
					,(Integer)result.get(i).get("country_id"));
			cars.add(tempCar);
		}
		String json = new Gson().toJson(cars.get(0));
		return json;
	}
	
	public String selectAll() {
		String query = "SELECT * FROM car_manufacturer";
		List<Object> params = new ArrayList<Object>();
		List<Map<String, Object>> result = select(query, params);
		List<CarManufacturer> cars = new ArrayList<CarManufacturer>();
		for (int i = 0; i < result.size(); i++) {
			CarManufacturer tempCar = new CarManufacturer((Integer)result.get(i).get("car_id")
					,(String)result.get(i).get("name")
					,(String)result.get(i).get("desc")
					,(Integer)result.get(i).get("country_id"));
			cars.add(tempCar);
		}
		String json = new Gson().toJson(cars);
		return json;
	}
	
	public void insert(CarManufacturer car_man) {
		String procedureName = "insert_car_manufacturer(?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(car_man.getName());
		params.add(car_man.getDesc());
		params.add(car_man.getCountryId());
		runStoredProcedure(procedureName, params);
	}
	
	public void update(CarManufacturer car_man) {
		String procedureName = "update_car_manufacturer(?, ?, ?, ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(car_man.getId());
		params.add(car_man.getName());
		params.add(car_man.getDesc());
		params.add(car_man.getCountryId());
		runStoredProcedure(procedureName, params);
	}
	
	public void delete(Integer car_man_id) {
		String procedureName = "delete_car_manufacturer(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(car_man_id);
		runStoredProcedure(procedureName, params);
	}
}
