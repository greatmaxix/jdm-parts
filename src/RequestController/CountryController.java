package RequestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import model.Country;
import table.CountryTable;

public class CountryController implements HttpHandler {

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		CountryTable ctable = new CountryTable();
		String respText;
		String noParams = "";
		Integer responseCode;
		exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		if (exchange.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
			exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE");
			exchange.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type,Authorization");
			exchange.sendResponseHeaders(204, -1);
            return;
        }
		//Read
		if ("GET".equals(exchange.getRequestMethod())) {
            Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
            String countryid = params.getOrDefault("countryid", List.of(noParams)).stream().findFirst().orElse(noParams);
            if (countryid.isEmpty()) {
            	respText = String.format(ctable.selectAll());
            }
            else {
            	respText = String.format(ctable.select(Integer.parseInt(countryid)));
            }
            responseCode = 200;
            exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
        }
		//Update
		else if ("POST".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
			InputStream is = exchange.getRequestBody();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				Country countryObj = new Gson().fromJson(jsonBody, Country.class);
				ctable.update(countryObj);
				respText = "Updated successfully!";
				responseCode = 200;
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		//Insert
		else if ("PUT".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
			InputStream is = exchange.getRequestBody();
			try {
				int i;
			    char c;
			    String jsonBody = "";
				while((i = is.read())!=-1) {
			        c = (char)i;
			        jsonBody += c;
			    }
				Country countryObj = new Gson().fromJson(jsonBody, Country.class);
				ctable.insert(countryObj);
				respText = "Inserted successfully!";
				responseCode = 200;
			}
			catch(Exception ex) {
				ex.printStackTrace();
				respText = "Error happened!";
				responseCode = 420;
			}
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		//Delete
		else if ("DELETE".contentEquals(exchange.getRequestMethod())) {
			Map<String, List<String>> params = HelperController.splitQuery(exchange.getRequestURI().getRawQuery());
            String countryid = params.getOrDefault("countryid", List.of(noParams)).stream().findFirst().orElse(noParams);
            try {
            	ctable.delete(Integer.parseInt(countryid));;
            	respText = "Deleted successfully!";
            	responseCode = 200;
            }
            catch(Exception ex) {
            	ex.printStackTrace();
            	respText = "Error occured!";
            	responseCode = 420;
            }
			exchange.sendResponseHeaders(responseCode, respText.getBytes().length);
            OutputStream output = exchange.getResponseBody();
            output.write(respText.getBytes());
            output.flush();
		}
		else {
            exchange.sendResponseHeaders(405, -1);
        }
        exchange.close();
	}

}
