package database_handlers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public abstract class BaseDatabaseManager {
	// 1) we need to store the db info
		//HOST, NAME, USER, PASS, PORT
	private String host;
	private String name;
	private String user;
	private String pass;
	private String port;
	private String driver;
	private String server_time_zone;
	
	protected String getHost() {
		return this.host;
	}
	
	protected String getName() {
		return this.name;
	}
	
	protected String getPort() {
		return this.port;
	}
	
	protected String getTimeZone() {
		return server_time_zone;
	}
	
	protected abstract String getDatabaseUrl();
	
	protected BaseDatabaseManager(
			String host,
			String name,
			String user,
			String pass,
			String port, 
			String server_time_zone) {
		this.host = host;
		this.name = name;
		this.user = user;
		this.pass = pass;
		this.port = port;
		this.server_time_zone = server_time_zone;
	}
	
	protected BaseDatabaseManager(String FilePath) throws IOException {
		try {
			InputStream inStream = getClass().getClassLoader().getResourceAsStream(FilePath);
			Properties pf = new Properties();
		
			pf.load(inStream);
			this.host = pf.getProperty("HOST");
			this.name = pf.getProperty("NAME");
			this.user = pf.getProperty("USER");
			this.pass = pf.getProperty("PASS");
			this.port = pf.getProperty("PORT");
			this.driver = pf.getProperty("DRIVER");
			this.server_time_zone = pf.getProperty("SERVERTIMEZONE");
		}
		catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	private static final String defaultPropertyFileName = "config_db.properties";
	
	protected BaseDatabaseManager() throws IOException {
		this(defaultPropertyFileName);
	}
	
	public List<Map<String, Object>> select(String query, List<Object> params) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		Map<String, Object> row = null;
		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(getDatabaseUrl(), user, pass);
			PreparedStatement statement = conn.prepareStatement(query);
			for(int i = 0; i < params.size(); i++) {
				int parameterIndex = i + 1;
				statement.setObject(parameterIndex, params.get(i));
			}
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
		    Integer columnCount = metaData.getColumnCount();

		    while (result.next()) {
		        row = new HashMap<String, Object>();
		        for (int i = 1; i <= columnCount; i++) {
		            row.put(metaData.getColumnName(i), result.getObject(i));
		        }
		        resultList.add(row);
		    }
			result.close();
			statement.close();
			conn.close();
			
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultList;
	}

	public void dml(String query, List<Object> parameters) {
		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(getDatabaseUrl(), user, pass);
			PreparedStatement statement = conn.prepareStatement(query);
			for(int i = 0; i < parameters.size(); i++) {
				int parameterIndex = i + 1;
				statement.setObject(parameterIndex, parameters.get(i));
			}
			int affectedRows = statement.executeUpdate();
			statement.close();
			conn.close();
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void runStoredProcedure(String procedureName, List<Object> parameters) {
		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(getDatabaseUrl(), user, pass);
			String query = "{CALL " + procedureName + "}";
			CallableStatement stmt = conn.prepareCall(query);
			for(int i = 0; i < parameters.size(); i++) {
				int parameterIndex = i + 1;
				stmt.setObject(parameterIndex, parameters.get(i));
			}
			Integer numberOfAffectedRows = stmt.executeUpdate(); 
			stmt.close();
			conn.close();
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean checkSelect(String query, List<Object> parameters) {
		boolean success = true; 
		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(getDatabaseUrl(), user, pass);
			CallableStatement stmt = conn.prepareCall(query);
			for(int i = 0; i < parameters.size(); i++) {
				int parameterIndex = i + 1;
				stmt.setObject(parameterIndex, parameters.get(i));
			}
			ResultSet resultSet = stmt.executeQuery();
			if (resultSet.next() == false) {
				success = false;
			}
			stmt.close();
			conn.close();
		}
		catch(Exception ex) {
			success = false;
			ex.printStackTrace();
		}
		return success;
	}
}
