package model;

import java.sql.Date;

public class CarModel {

	private Integer model_id;
	private String name;
	private Integer car_man_id;
	private Date car_man_date;
	
	public CarModel(Integer model_id, String name, Integer car_man_id, Date car_man_date) {
		this.model_id = model_id;
		this.name = name;
		this.car_man_id = car_man_id;
		this.car_man_date = car_man_date;
	}
	
	public CarModel(String name, String desc, Integer car_man_id, Date car_man_date) {
		this.name = name;
		this.car_man_id = car_man_id;
		this.car_man_date = car_man_date;
	}

	public Integer getId() {
		return this.model_id;
	}
	

	public Integer getCarManId() {
		return this.car_man_id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Date getCarManDate() {
		return this.car_man_date;
	}
}
