package model;

public class CarManufacturer {
	private Integer car_man_id;
	private String name;
	private String desc;
	private Integer country_id;
	
	public CarManufacturer(Integer car_man_id, String name, String desc, Integer country_id) {
		this.car_man_id = car_man_id;
		this.name = name;
		this.desc = desc;
		this.country_id = country_id;
	}
	
	public CarManufacturer(String name, String desc, Integer country_id) {
		this.name = name;
		this.desc = desc;
		this.country_id = country_id;
	}

	public Integer getId() {
		return this.car_man_id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public Integer getCountryId() {
		return this.country_id;
	}
}
